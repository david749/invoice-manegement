 ## Aplikasi Invoice Management ##
 
Aplikasi ini digunakan untuk mengelola invoice dan menyambungkan dengan berbagai metode pembayaran masa kini.
Diantara metode pembayaran yang akan disupport 

* Virtual Account Bank
  * Bank BNI
  * Bank CIMB
  * Bank BSI

* E-Wallet
  * OVO 
  * GoPay

* QR Payment
  * QRIS

Tipe pembayaran yang tersedia:
  * CLOSED: Bayar sesuai nominal. Jika tidak sesuai ditolak
  * OPEN: Pembayaran berapun diterima
  * INSTALLMENT: Pembayaran diterima selama jumlah/akumulasi lebih kecil atau sama dengan nilai tagihan




Fitur Aplikasi:

* Manajemen Customer
    * Registrasi Customer
    * Rekap tagihan Customer
    * History pembayaran


* Manjemen Invoice
    * Membuat invoice
    * Mengganti nilai dan tanggal jatuh tempo
    * Membatalkan invoice




## Cara Setup Database ##

1. Jalankan Postgresql di docker

sudo docker run --rm \
  --name invoice-db \
  -e POSTGRES_DB=invoicedb \
  -e POSTGRES_USER=david \
  -e POSTGRES_PASSWORD=d4v1d4nw4r \
  -e PGDATA=/var/lib/postgresql/data/pgdata \
  -v "$PWD/invoicedb-data:/var/lib/postgresql/data" \
  -p 5431:5431 \
  postgres:13
