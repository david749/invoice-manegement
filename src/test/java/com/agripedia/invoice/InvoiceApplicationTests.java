package com.agripedia.invoice;

import com.agripedia.invoice.exceptions.VirtualAccountAlreadyPaidException;
import com.agripedia.invoice.exceptions.VirtualAccountNotFoundException;
import com.agripedia.invoice.services.PaymentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class InvoiceApplicationTests {
    @Autowired
    private PaymentService paymentService;

    @Test
    void testPay() throws VirtualAccountAlreadyPaidException, VirtualAccountNotFoundException {
        paymentService.pay(null, null, null, null, null);
    }

}
