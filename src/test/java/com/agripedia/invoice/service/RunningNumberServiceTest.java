package com.agripedia.invoice.service;

import com.agripedia.invoice.services.RunningNumberService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class RunningNumberServiceTest {

    @Autowired
    private RunningNumberService runningNumberService;

    @Test
    public void testGetNumber() {
        Long hasil = runningNumberService.getNumber("sandiku");
        Assertions.assertNotNull(hasil);
        System.out.println("Hasil : " + hasil);
    }

    @Test
    public void testGetNumberThread() throws InterruptedException {
        int jumlahThread = 10;
        final int iterasi = 5;
        for (int i = 0; i < jumlahThread; i++) {
            Thread t = new Thread() {
                @Override
                public void run() {
                    for (int j = 0; j < iterasi; j++) {
                        Long hasil = runningNumberService.getNumber("sandiku");
                        System.out.println("Thread [" + this.getId() + "] Last : " + hasil);
                    }

                }
            };
            t.start();
        }
        Thread.sleep(10 * 1000);
    }
}
