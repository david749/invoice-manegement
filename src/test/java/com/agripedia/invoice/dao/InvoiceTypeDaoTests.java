package com.agripedia.invoice.dao;

import com.agripedia.invoice.entity.InvoiceType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@Sql(scripts = { "/sql/delete-invoice-type.sql", "/sql/insert-inactive-invoice-type.sql" })
public class InvoiceTypeDaoTests  {

    @Autowired InvoiceTypeDao invoiceTypeDao;

    @Test
    public void testInsertInvoiceType() throws Exception {
        InvoiceType it = new InvoiceType();
        it.setName("Invoice Type Test");
        it.setCode("IT-001");
        Assertions.assertNull(it.getId());
        invoiceTypeDao.save(it);
        System.out.println("ID : " + it.getId());
        System.out.println("Create : " +  it.getCreated());
        System.out.println("Created By: " + it.getCreatedBy());
        System.out.println("Update: " + it.getUpdated());
        System.out.println("Updated By: " + it.getUpdatedBy());
        System.out.println("Status Record: " + it.getStatusRecord());
        Assertions.assertNotNull(it.getId());
        Assertions.assertEquals(it.getCreated(), it.getUpdated());


//        Thread.sleep(1000, 1000);
//        it.setName("Test User Updated");
//        invoiceTypeDao.save(it);
//        System.out.println("ID : " + it.getId());
//        System.out.println("Create : " +  it.getCreated());
//        System.out.println("Update: " + it.getUpdated());
//        Assertions.assertNotEquals(it.getCreated(), it.getUpdated());

    }

    @Test
    public void testQuerySoftDelete() {
        Long jumlahRecord = invoiceTypeDao.count();
        System.out.println("Jumlah Record: " + jumlahRecord);

    }

    @Test
    public void testSoftDelete() {
        InvoiceType it = invoiceTypeDao.findById("tes002").get();
        invoiceTypeDao.delete(it);
        Long jumlahRecord = invoiceTypeDao.count();
        System.out.println("Jumlah Record: " + jumlahRecord );
        Assertions.assertEquals(0, jumlahRecord);
    }
}
