insert into invoice_type(id, code, name, created, created_by, updated, updated_by, status_record)
values ('tes001', 'T-001', 'Test 001', '2021-08-03 11:58:21.034562', 'User Tes 1', '2021-08-03 11:58:22.034562', 'User Tes 1', 'INACTIVE');

insert into invoice_type(id, code, name, created, created_by, updated, updated_by, status_record)
values ('tes002', 'T-002', 'Test 002', '2021-08-03 11:58:21.034562', 'User Tes 2', '2021-08-03 11:58:22.034562', 'User Tes 2', 'ACTIVE');
