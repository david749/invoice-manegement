create table running_number (
    id character varying(36) NOT NULL,
    prefix character varying(100) NOT NULL,
    last_number bigint NOT NULL
);

ALTER TABLE ONLY running_number
    ADD CONSTRAINT running_number_pkey PRIMARY KEY (id);

create table customer (
    id character varying(36) NOT NULL,
    code character varying(100) NOT NULL,
    name character varying(100) NOT NULL,
    email character varying(100) NOT NULL,
    mobile_phone character varying(30) NOT NULL,
    created timestamp without time zone,
    created_by character varying(255),
    status_record character varying(10),
    updated timestamp without time zone,
    updated_by character varying(255)
);

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id);

create table payment_provider (
    id character varying(36) NOT NULL,
    code character varying(100) NOT NULL,
    name character varying(100) NOT NULL,
    created timestamp without time zone,
    created_by character varying(255),
    status_record character varying(10),
    updated timestamp without time zone,
    updated_by character varying(255),
    logo character varying(255)
);

ALTER TABLE ONLY payment_provider
    ADD CONSTRAINT payment_provider_pkey PRIMARY KEY (id);

alter table payment_provider
    add constraint payment_provider_unique_code unique (code);

create table invoice_type (
    id character varying(36) NOT NULL,
    created timestamp without time zone,
    created_by character varying(255),
    status_record character varying(10),
    updated timestamp without time zone,
    updated_by character varying(255),
    code character varying(255),
    name character varying(255)

);

ALTER TABLE ONLY invoice_type
    ADD CONSTRAINT invoice_type_pkey PRIMARY KEY (id);


create table invoice_type_provider (
    id_invoice_type character varying(36) NOT NULL,
    id_payment_provider character varying(36) NOT NULL
);

ALTER TABLE ONLY invoice_type_provider
    ADD CONSTRAINT invoice_type_provider_pkey PRIMARY KEY (id_invoice_type, id_payment_provider);

ALTER TABLE ONLY invoice_type_provider
    ADD CONSTRAINT fk_invoice_type_provider_type FOREIGN KEY (id_invoice_type) REFERENCES invoice_type(id);

ALTER TABLE ONLY invoice_type_provider
    ADD CONSTRAINT fk_invoice_type_provider_provider FOREIGN KEY (id_payment_provider) REFERENCES payment_provider(id);


create table invoice (
    id character varying(36) NOT NULL,
    created timestamp without time zone,
    created_by character varying(255),
    status_record character varying(10),
    updated timestamp without time zone,
    updated_by character varying(255),
    amount numeric(19,2),
    description character varying(255),
    due_date date,
    invoice_number character varying(255),
    paid boolean,
    id_invoice_type character varying(255),
    id_customer character varying(255) NOT NULL
);

ALTER TABLE ONLY invoice
    ADD CONSTRAINT invoice_pkey PRIMARY KEY (id);

alter table invoice
    add constraint invoice_unique_number unique (invoice_number);

ALTER TABLE ONLY invoice
    ADD CONSTRAINT fkco4sbxv9cj2oevm6cdpq76ffb FOREIGN KEY (id_invoice_type) REFERENCES invoice_type(id);

ALTER TABLE ONLY invoice
    ADD CONSTRAINT fk_invoice_customer FOREIGN KEY (id_customer) REFERENCES customer(id);

create table virtual_account (
    id character varying(36) NOT NULL,
    created timestamp without time zone,
    created_by character varying(255),
    status_record character varying(10),
    updated timestamp without time zone,
    updated_by character varying(255),
    account_number character varying(255),
    company_id character varying(255),
    virtual_account_type integer,
    id_invoice character varying(255),
    id_payment_provider character varying(255)
);

ALTER TABLE ONLY virtual_account
    ADD CONSTRAINT virtual_account_pkey PRIMARY KEY (id);

ALTER TABLE ONLY virtual_account
    ADD CONSTRAINT fkt3t7f64hvgk4xjblsovqqkpll FOREIGN KEY (id_payment_provider) REFERENCES payment_provider(id);

ALTER TABLE ONLY virtual_account
    ADD CONSTRAINT fkbbdwdxpgdisiikyyhf2xteblc FOREIGN KEY (id_invoice) REFERENCES invoice(id);



create table payment (
    id character varying(36) NOT NULL,
    created timestamp without time zone,
    created_by character varying(255),
    status_record character varying(10),
    updated timestamp without time zone,
    updated_by character varying(255),
    amount numeric(19,2),
    provider_reference character varying(255),
    transaction_time timestamp without time zone,
    id_virtual_account character varying(255)
);

ALTER TABLE ONLY payment
    ADD CONSTRAINT payment_pkey PRIMARY KEY (id);

ALTER TABLE ONLY payment
    ADD CONSTRAINT fkptriq88d7e8io9mhri8p10cq0 FOREIGN KEY (id_virtual_account) REFERENCES virtual_account(id);
