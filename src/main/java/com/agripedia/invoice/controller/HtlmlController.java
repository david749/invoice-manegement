package com.agripedia.invoice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Slf4j
public class HtlmlController {

    @PreAuthorize("hasAuthority('EDIT_TRANSAKSI')")
    @GetMapping("/home")
    public void home(Authentication authentication) {
        log.info("Jenis class authentication {}", authentication.getClass().getSimpleName());
        log.info("User yang sedang login {}", authentication.getPrincipal());
    }
}
