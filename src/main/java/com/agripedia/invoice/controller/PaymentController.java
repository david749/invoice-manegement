package com.agripedia.invoice.controller;

import com.agripedia.invoice.exceptions.VirtualAccountAlreadyPaidException;
import com.agripedia.invoice.exceptions.VirtualAccountNotFoundException;
import com.agripedia.invoice.services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    public void pay() throws VirtualAccountAlreadyPaidException, VirtualAccountNotFoundException {
        paymentService.pay(null, null, null, null, null);
    }
}
