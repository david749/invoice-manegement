package com.agripedia.invoice.exceptions;

// RuntimeException digunakan untuk error yang tidak bisa dihandle oleh yang manggil
// (controller api, controller web, ios8583 handler)
// contoh: select one tapi yang ada di database > 1
// ini permasalahan di sistem dan user tidak mungkin bisa menghandle
public class VirtualAccountNotFoundException extends Exception {
    public VirtualAccountNotFoundException() {
    }

    public VirtualAccountNotFoundException(String message) {
        super(message);
    }
}
