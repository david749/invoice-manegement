package com.agripedia.invoice.exceptions;

// jangan pakai RuntimeException agar wajib dihandle oleh manggil
// karena ini bisa dihadle oleh user
public class VirtualAccountAlreadyPaidException extends Exception {

    public VirtualAccountAlreadyPaidException() {
    }
    public VirtualAccountAlreadyPaidException(String message) {
        super(message);
    }
}
