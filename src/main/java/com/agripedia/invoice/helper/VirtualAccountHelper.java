package com.agripedia.invoice.helper;

import com.agripedia.invoice.dao.VirtualAccountDao;
import com.agripedia.invoice.entity.PaymentProvider;
import com.agripedia.invoice.entity.VirtualAccount;
import com.agripedia.invoice.exceptions.VirtualAccountNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class VirtualAccountHelper {
    @Autowired
    private VirtualAccountDao virtualAccountDao;

    public VirtualAccount cekVaAda(PaymentProvider provider, String companyId, String accountNumber) throws VirtualAccountNotFoundException {
        Optional<VirtualAccount> optVa = virtualAccountDao.findByPaymentProviderAndCompanyIdAndAccountNumber(
                provider, companyId, accountNumber
        );
        if (!optVa.isPresent()) {
            throw new VirtualAccountNotFoundException("VA [" + companyId + " / " + accountNumber +
                    " - " + provider.getCode() + " ] not found"
            );
        }
        VirtualAccount va = optVa.get();
        return va;
    }

}
