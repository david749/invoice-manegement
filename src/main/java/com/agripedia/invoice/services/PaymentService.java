package com.agripedia.invoice.services;

import com.agripedia.invoice.dao.VirtualAccountDao;
import com.agripedia.invoice.entity.Invoice;
import com.agripedia.invoice.entity.PaymentProvider;
import com.agripedia.invoice.entity.VirtualAccount;
import com.agripedia.invoice.exceptions.PaymentExceedInvoiceAmountException;
import com.agripedia.invoice.exceptions.VirtualAccountAlreadyPaidException;
import com.agripedia.invoice.exceptions.VirtualAccountNotFoundException;
import com.agripedia.invoice.helper.VirtualAccountHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Optional;


@Service @Transactional(rollbackFor = {
        VirtualAccountAlreadyPaidException.class,
        VirtualAccountNotFoundException.class
})
public class PaymentService {

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private VirtualAccountDao virtualAccountDao;

    @Autowired
    private VirtualAccountHelper virtualAccountHelper;

    public void pay(PaymentProvider provider, String companyId,
                    String accountNumber, BigDecimal amount, String reference) throws VirtualAccountNotFoundException, VirtualAccountAlreadyPaidException {

        // begin tx1
        auditLogService.log("Start transaction log " + accountNumber); // suspend tx1 dalam method log

        VirtualAccount va = virtualAccountHelper.cekVaAda(provider, companyId, accountNumber);
        cekVaLunas(provider, companyId, accountNumber, va);

        // 3. cek apakah akumulasi pembayaran < amount
        // 4. update status VA mejadi lunas
        Invoice invoice = va.getInvoice();

        // 5. update status invoice menjadi lunas
        // 6. insert ke tabel payment
        // 7. notifikasi (next fase)


        // commit tx2
    }

    private void cekVaLunas(PaymentProvider provider, String companyId, String accountNumber, VirtualAccount va) throws VirtualAccountAlreadyPaidException {
        if (va.getInvoice().getPaid()) {
            throw new VirtualAccountAlreadyPaidException("VA [" + companyId + " / " + accountNumber +
                    " - " + provider.getCode() + " ] already paid");
        }
    }


}
