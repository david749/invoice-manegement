package com.agripedia.invoice.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AuditLogService {

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void log(String log) {
        // suspend transaction yang sedang berjalan (tx1)
        // start transaction baru (tx1)
        // yang ada di sini akan dijalankan dalam trasaction baru (tx2)
        // commit rollback tx2
        // resume tx1
    }

}
