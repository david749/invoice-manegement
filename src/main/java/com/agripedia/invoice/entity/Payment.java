package com.agripedia.invoice.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity @Data
public class Payment extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_virtual_account")
    private VirtualAccount virtualAccount;

    private LocalDateTime transactionTime;

    private String providerReference;

    private BigDecimal amount;
}
