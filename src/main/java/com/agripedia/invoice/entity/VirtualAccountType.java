package com.agripedia.invoice.entity;

public enum VirtualAccountType {
    CLOSED, OPEN, INSTALLMENT
}
