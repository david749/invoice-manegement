package com.agripedia.invoice.entity;

import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity @Data
@SQLDelete(sql = "UPDATE virtual_account SET status_record = 'INACTIVE' WHERE id=?")
@Where(clause = "status_record = 'ACTIVE'")
public class VirtualAccount extends BaseEntity {

    @ManyToOne @JoinColumn(name = "id_payment_provider")
    private PaymentProvider paymentProvider;

    @ManyToOne @JoinColumn(name = "id_invoice")
    private Invoice invoice;

    private String companyId;
    private String accountNumber;
    private VirtualAccountType virtualAccountType = VirtualAccountType.CLOSED;

}
