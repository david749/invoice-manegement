package com.agripedia.invoice.entity;

public enum StatusRecord {
    ACTIVE, INACTIVE
}
